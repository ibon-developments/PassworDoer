var importedFile = '';
let environment = require('environment_vars');


//LOCALFORAGE CONFIGURATION
localforage.setDriver([localforage.INDEXEDDB,localforage.WEBSQL,localforage.LOCALSTORAGE]);


//////////////////////////////////////////////////////////////////////// BEFORE LOADING THE PAGE, TOKEN AND KEYSTORES ARE CHECKED ///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//THIS FUNCTION CHECKS THE TOKEN VALIDITY AGAINST THE BACKEND
async function check_token(token) {
  //START ENDPOINT CALL
  const url = script_root + '/gettoken';
  const param = {
    headers : {"Authorization" : token},
    method: "GET",
    json:true
  };
  let response = await fetch(url,param);
  let response_json = await response.json();
  console.log(response_json);
  return response_json;
}

//GET TOKEN FROM LOCALFORAGE
async function checkTokenKeystore() {
  let token = await localforage.getItem('token');
  console.log('el token es: Bearer ' + token);
  if (token != null) {
    let checktoken = await await check_token('Bearer ' + token);
    var answer = checktoken['answer'];
    let username = checktoken['username'];
    if( answer == 42) {
      document.getElementById('signin').style.display = 'none';
      document.getElementById('dentro').style.display = 'block';
      document.getElementById('user').innerHTML = username;
      document.getElementById('token').innerHTML = token;
    }
    else {
      let keystore = await localforage.getItem('keystore');
      if(keystore != null) {
        document.getElementById('select_wallet_btn').style.display = 'none';
      }
      else {
        document.getElementById('select_wallet_btn').style.display = 'flex';
      }
      document.getElementById('signin').style.display = 'flex';
      document.getElementById('dentro').style.display = 'none';
    }
  }
  else {
    let keystore = await localforage.getItem('keystore');
    if(keystore != null) {
      document.getElementById('select_wallet_btn').style.display = 'none';
    }
    else {
      document.getElementById('select_wallet_btn').style.display = 'flex';
    }
    document.getElementById('signin').style.display = 'flex';
    document.getElementById('dentro').style.display = 'none';
  }
}

//BEFORE LOADING THE PAGE WE WILL CHECK WHETHER THERE IS A TOKEN IN LOCALFORAGE. IF THERE IS, THE USER WILL GET IN THE APPLICATION
//WITHOUT NEEDING TO LOG IN
checkTokenKeystore();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////// END OF CHECK TOKEN AND KEYSTORE //////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// AFTER LOADING PAGE -- CONNECTION TO INFURA ///////////////////////////////////////////
//ON LOAD PAGE
window.addEventListener('load', function() {
  //empty all textboxes
  var elements = document.getElementsByTagName("input");
  for (var ii=0; ii < elements.length; ii++) {
    if (elements[ii].type == "text") {
      elements[ii].value = "";
    }
  }
  //connection to infura
  web3 = new Web3(new Web3.providers.HttpProvider(environment.http_provider));

})
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////  END AFTER LOADING PAGE -- CONNECTION TO INFURA //////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// LOG OUT //////////////////////////////////////////////////////////////////////////////
async function logout() {
  //API logout using fetch
  var token = await localforage.getItem('token');
  token = 'Bearer ' + token;
  const url = script_root + '/logout';
  const param = {
    headers : {"Authorization" : token},
    method: "POST",
    json: true
  };
  fetch(url,param)
  .then(res => res.json())
  .then(async function(data){
      //registration div is hidden
      let keystore = await localforage.getItem('keystore');
      if(keystore != null) {
        document.getElementById('select_wallet_btn').style.display = 'none';
      }
      else {
        document.getElementById('select_wallet_btn').style.display = 'flex';
      }
      document.getElementById('signin').style.display = 'flex';
      document.getElementById('dentro').style.display = 'none';
  })
  .catch(error => console.log('3.esto es'+error))
}




/////////////////////////////////////////////////////////// THIS IS HERE FOR TESTING PURPOSES - REMOVE TOKEN AND KEYSTORE ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function remove_keystore() {
  localforage.removeItem('keystore',function(err,result){
    if (err) {
      console.error('Error writting to localforage');
    }
    else {
      console.log('2. keystore: ' + JSON.stringify(result) + ' removed from localforage');
      console.log('=================================================');
    }
  });
}
function remove_token() {
  localforage.removeItem('token',function(err,result){
    if (err) {
      console.error('Error writting to localforage');
    }
    else {
      console.log('2. token: ' + JSON.stringify(result) + ' removed localforage');
      console.log('=================================================');
    }
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// END OF REMOVE TOKEN AND KEYSTORE /////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////// REGISTRATION ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCTION THAT TRIGGERS WHEN USER CLICKS ON "NOT AN ACCOUNT"
function doGoRegister() {
  document.getElementById('signin').style.display = 'none';
  document.getElementById('register').style.display = 'flex';
}


async function signup() {
  pwd = document.getElementById('signup_password').value;
  pwd1 = document.getElementById('confirm_signup_password').value;
  // check that pwd is not empty and also that both are the same
  if (pwd != '' && pwd == pwd1) {
    //create eth account
    var account = doCreate();
    //Encrypt keystore and write to localforage
    doEncrypt(account['privateKey'],pwd);
    //API UserRegistration
    const url = script_root + '/registration';
    const data = {
      username : account['address'],
      password : pwd
    };
    const param = {
      headers : {"content-type" : "application/json;charset=UTF-8"},
      body:JSON.stringify(data),
      method: "POST",
      json:true
    };
    fetch(url,param)
    .then(res => res.json())
    .then(async function(data){
      await save_to_local_forage(data['access_token'],'token');
    })
    .catch(error => console.log('1.esto es'+error))

    //registration div is hidden
    document.getElementById('register').style.display = 'none';
    //download file div is displayed
    document.getElementById('bk_wallet').style.display = 'flex';
  }
  else {
    alert('Password is empty');
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF REGISTRATION ////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////// ENCRYPTION ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ENCRYPT PRIVATE KEY AND SAVE IN LOCALFORAGE
async function doEncrypt(pk,pw) {
  encrypted_private_key = web3.eth.accounts.encrypt(pk,pw);
  console.log(encrypted_private_key);
  await localforage.setItem('keystore',encrypted_private_key);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF ENCRYPTION //////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////// CREATE ACCOUNT ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CREATE ACCOUNT
function doCreate() {
  account_to_create = web3.eth.accounts.create();
  return account_to_create;
}



///////////////////////////////////////////////////////////////////////// KEYSTORE DOWNLOAD ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 7. BACKUP WALLET
function bk_wallet() {
  downloadFile('file.json',JSON.stringify(encrypted_private_key));
}

// 8. DOWNLOAD FILE
function downloadFile(filename,text) {
  var element = document.createElement('a');
  element.setAttribute('href','data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download',filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF KEYSTORE DOWNLOAD ///////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////// SIGNIN AND DECRYPT /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LOGIN
async function signin() {
  //login is done if pwd text box is not empty
  if (document.getElementById('signin_password').value != '') {
    //if importedFile exists, it is sent to do the decrypt
    if (importedFile !=''){
      doDecrypt(JSON.parse(importedFile),document.getElementById('signin_password').value);
    }
    //if importedFile does not exist, the keystore in localforage is sent to do the decrypt
    else {
      let keystore = await localforage.getItem('keystore');
      await doDecrypt(keystore,document.getElementById('signin_password').value);
    }
  }
  else {
    alert('Password is empty');
  }
}

//DECRYPT PRIVATE KEY
async function doDecrypt(keystore,p) {
  decrypted_private_key = web3.eth.accounts.decrypt(keystore, p)
  //API login using fetch
  const url = script_root + '/login';
  const data = {
    username : decrypted_private_key['address'],
    password : p
  };
  const param = {
    headers : {"content-type" : "application/json;charset=UTF-8"},
    body:JSON.stringify(data),
    method: "POST",
    json:true
  };
  fetch(url,param)
  .then(res => res.json())
  .then(async function(data){
    if(data['access_token'] != 'NA') {
      await save_to_local_forage(data['access_token'],'token');
      if (importedFile != ''){
        doEncrypt(decrypted_private_key['privateKey'],p);
      }
      //registration div is hidden
      document.getElementById('signin').style.display = 'none';
      //download file div is displayed
      document.getElementById('dentro').style.display = 'block';
      document.getElementById('user').innerHTML = data['username'];
      document.getElementById('token').innerHTML = data['access_token'];
    }
    else {
      alert(data['message']);
    }
  })
  .catch(error => console.log('3.esto es'+error))
}



// 4. IMPORT THE KEYSTORE
//if the element file-input changes, that means that the import keystore button has been clicked and that triggers readSingleFile
document.getElementById('file-input').addEventListener('change', readSingleFile, false);
function readSingleFile(evt) {
  // importedFile is emptied
  importedFile = '';
  // Check for the various File API support.
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0];
    if (f) {
      var r = new FileReader();
      r.onload = function(e) {
	      importedFile = e.target.result;
      }
      r.readAsText(f);
    } else {
      alert("Failed to load file");
    }
  }
  else {
    alert('The File APIs are not fully supported by your browser.');
  }
}


function save_to_local_forage(d,n) {
  console.log(d);
  console.log(n);
  if(n == 'token' & d != 'NA') {
    localforage.setItem(n,d,function(err,result){
      if (err) {
        console.error('Error writting to localforage');
      }
      else {
        console.log('2. token: ' + JSON.stringify(result) + ' written to localforage');
        console.log('=================================================');
      }
    });
  }
  else{
    console.log('no hay token');
  }

}







//UNLOCKACCOUNT --> NOT BEING USED
function    doUnlockAccount()  {
  web3.personal.unlockAccount(account, pwd,function(error, result)  {
        // console.log(error,result)
        if(error){
            alert('Unlock error ' + error);
            //setData('lock_unlock_result',error,true);
        } else {
            // Result = True if unlocked, else false
            var str = account.substring(0,20)+'...Unlocked';
            if(result){
                console.log('Unlock: '+ str);
                //setData('lock_unlock_result',str,false);
            } else {
                // This does not get called - since and error is returned for incorrect password :-)
                str = 'Incorrect Password???';
                console.log('Unlock: '+ str);
                //setData('lock_unlock_result',str,true);
            }
        }
    });
}
