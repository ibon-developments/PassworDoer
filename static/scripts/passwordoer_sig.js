var sign_string = 'Hello World';
var account_to_create = '';
var acccount = '';
var privatekey = '';
var encrypted_private_key = '';
var decrypted_private_key = '';
var pwd = '';
var pwd1 = '';
var importedFile = '';
var token ='';

//LOCALFORAGE CONFIGURATION
localforage.setDriver([localforage.INDEXEDDB,localforage.WEBSQL,localforage.LOCALSTORAGE]);


//////////////////////////////////////////////////////////////////////// BEFORE LOADING THE PAGE, TOKEN AND KEYSTORES ARE CHECKED ///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//THIS FUNCTION CHECKS THE TOKEN VALIDITY AGAINST THE BACKEND
async function check_token(token) {
  //START ENDPOINT CALL
  const url = script_root + '/gettoken';
  const param = {
    headers : {"Authorization" : token},
    method: "GET",
    json:true
  };
  let response = await fetch(url,param);
  let response_json = await response.json();
  console.log(response_json);
  return response_json;
}

//GET TOKEN FROM LOCALFORAGE
async function checkTokenKeystore() {
  let token = await localforage.getItem('token');
  console.log('Token Bearer ' + token);

  if (token != null) {
    console.log('Token available');
    let checktoken = await await check_token('Bearer ' + token);
    var answer = checktoken['answer'];
    let username = checktoken['username'];
    let words_keystore = await localforage.getItem('words_keystore');
    let keystore = await localforage.getItem('keystore');
    let wallet_keystore = await localStorage.getItem('web3js_wallet');
    console.log('Local storage words keystore');
    console.log(words_keystore);
    console.log('Local storage address keystore');
    console.log(keystore);
    console.log('Local storage wallet keystore');
    console.log(wallet_keystore);
    if(!words_keystore) {
      console.log('No words keystore stored');
      document.getElementById('words_form').style.display = 'none';
    } else {
      console.log('Words keystore stored');
      document.getElementById('words_form').style.display = 'flex';
    }
    if( answer == 42) {
      console.log('Token is valid');
      document.getElementById('signin').style.display = 'none';
      document.getElementById('dentro').style.display = 'block';
      document.getElementById('user').innerHTML = username;
      document.getElementById('token').innerHTML = token;
      getAccountBalance("balance", web3, username);
    }
    else {
      console.log('Not valid token');
      //let keystore = await localforage.getItem('keystore');
      //console.log(keystore);
      document.getElementById('wallet_stored').style.display = 'none';
      document.getElementById('address_stored').style.display = 'none';
      console.log(keystore);
      console.log(wallet_keystore);
      if(!keystore && !wallet_keystore) {
        console.log('No address keystore or wallet keystore stored');
        document.getElementById('select_wallet_btn').style.display = 'flex';
      }
      if(wallet_keystore) {
        console.log('Wallet keystore stored');
        document.getElementById('select_wallet_btn').style.display = 'none';
        document.getElementById('wallet_stored').style.display = 'flex';
      }else{
        console.log('No wallet keystore stored');
        document.getElementById('select_wallet_btn').style.display = 'flex';
      }
      if(keystore) {
        console.log('Address keystore stored');
        document.getElementById('select_wallet_btn').style.display = 'none';
        document.getElementById('address_stored').style.display = 'flex';
      }
      if(words_keystore) {
        console.log('Words keystore stored');
        document.getElementById('words_stored').style.display = 'flex';
      }else{
        console.log('No words keystore stored');
        document.getElementById('words_stored').style.display = 'none';
      }
      document.getElementById('signin').style.display = 'flex';
      document.getElementById('dentro').style.display = 'none';
    }
  }
  else {
    let keystore = await localforage.getItem('keystore');
    console.log('No token');
    console.log('Keystore:');
    console.log(keystore);
    if(keystore != null) {
      document.getElementById('select_wallet_btn').style.display = 'none';
    }
    else {
      document.getElementById('select_wallet_btn').style.display = 'flex';
    }
    document.getElementById('signin').style.display = 'flex';
    document.getElementById('dentro').style.display = 'none';
  }
}
//BEFORE LOADING THE PAGE WE WILL CHECK WHETHER THERE IS A TOKEN IN LOCALFORAGE. IF THERE IS, THE USER WILL GET IN THE APPLICATION
//WITHOUT NEEDING TO LOG IN
checkTokenKeystore();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////// END OF CHECK TOKEN AND KEYSTORE //////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// AFTER LOADING PAGE -- CONNECTION TO INFURA ///////////////////////////////////////////
// 0. LOAD PAGE
window.addEventListener('load', function() {
  //empty all textboxes
  var elements = document.getElementsByTagName("input");
  for (var ii=0; ii < elements.length; ii++) {
    if (elements[ii].type == "text") {
      elements[ii].value = "";
    }
  }
  //connection to infura
  console.log(environment.http_provider);
  web3 = new Web3(new Web3.providers.HttpProvider(environment.http_provider));
  //RSK
  //web3 = new Web3(new Web3.providers.HttpProvider('https://public-node.testnet.rsk.co/'));
  //web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:4444'));
  console.log(web3);
})
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////  END AFTER LOADING PAGE -- CONNECTION TO INFURA //////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// LOG OUT //////////////////////////////////////////////////////////////////////////////
async function logout() {
  //API logout using fetch
  var token = await localforage.getItem('token');
  token = 'Bearer ' + token;
  const url = script_root + '/logout';
  const param = {
    headers : {"Authorization" : token},
    method: "POST",
    json: true
  };
  fetch(url,param)
  .then(res => res.json())
  .then(async function(data){
      //registration div is hidden
      checkTokenKeystore();

      //let keystore = await localforage.getItem('keystore');
      //console.log('Keystore:');
      //console.log(keystore);
      //if(keystore != null) {
        //console.log('To none');
        //document.getElementById('select_wallet_btn').style.display = 'none';
      //}
      //else {
        //console.log('To flex');
        //document.getElementById('select_wallet_btn').style.display = 'flex';
      //}
      //document.getElementById('signin').style.display = 'flex';
      //document.getElementById('dentro').style.display = 'none';
  })
  .catch(error => console.log('Error '+error))
}

/////////////////////////////////////////////////////////// THIS IS HERE FOR TESTING PURPOSES - REMOVE TOKEN AND KEYSTORE ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function remove_words() {
  localforage.removeItem('words_keystore',function(err,result){
    if (err) {
      console.error('Error writting to localforage');
    }
    else {
      console.log('Words keystore removed from localforage');
      console.log('=================================================');
    }
  });
  checkTokenKeystore();
}

function remove_keystore() {
  localforage.removeItem('keystore',function(err,result){
    if (err) {
      console.error('Error writting to localforage');
    }
    else {
      console.log('Keystore removed from localforage');
      console.log('=================================================');
    }
  });
  checkTokenKeystore();
}
function remove_token() {
  localforage.removeItem('token',function(err,result){
    if (err) {
      console.error('Error writting to localforage');
    }
    else {
      console.log('Token: ' + JSON.stringify(result) + ' removed from localforage');
      console.log('=================================================');
    }
  });
  checkTokenKeystore();
}

function remove_wallet() {
  localStorage.removeItem('web3js_wallet');
  console.log('Wallet keystore removed');
  //Securely empty the wallet element
  web3.eth.accounts.wallet.clear();
  checkTokenKeystore();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////// END OF REMOVE TOKEN AND KEYSTORE /////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////// REGISTRATION ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FUNCTION THAT TRIGGERS WHEN USER CLICKS ON "NOT AN ACCOUNT"
function doGoRegister() {
  document.getElementById('signin').style.display = 'none';
  document.getElementById('register').style.display = 'flex';
}


async function signup() {
  pwd = document.getElementById('signup_password').value;
  pwd1 = document.getElementById('confirm_signup_password').value;
  // check that pwd is not empty and also that both are the same
  if (pwd != '' && pwd == pwd1) {
    //create eth account
    var accountCreation = doCreate(pwd);
    var wallet = accountCreation.wallet;
    var address = accountCreation.address;
    var privatekey = accountCreation.privatekey;
    var words = accountCreation.words;
    //Setting the default account
    web3.eth.defaultAccount = address;
    console.log('Wallet with address at 0: ' + wallet[0]['address']);


    //Encrypt keystore and words and write them to localforage
    doEncrypt(privatekey,pwd,words);
    //API UserRegistration
    const url = script_root + '/registration';
    const data = {
      username : address
    };
    const param = {
      headers : {"content-type" : "application/json;charset=UTF-8"},
      body:JSON.stringify(data),
      method: "POST",
      json:true
    };
    fetch(url,param)
    .then(res => res.json())
    .then(async function(data){
      await save_to_local_forage(data['access_token'],'token');
    })
    .catch(error => console.log('Error '+error))

    //registration div is hidden
    document.getElementById('register').style.display = 'none';
    //download file div is displayed
    document.getElementById('bk_user').innerHTML = address;
    getAccountBalance("bk_balance", web3, address);
    document.getElementById('bk_words').innerHTML = words;
    document.getElementById('bk_privatekey').innerHTML = privatekey;
    //getAccountWords("bk_words", "english");
    document.getElementById('bk_wallet').style.display = 'flex';
  }
  else {
    alert('Password is empty');
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF REGISTRATION ////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////// ENCRYPTION ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ENCRYPT PRIVATE KEY AND SAVE IN LOCALFORAGE
async function doEncrypt(pk,pw,words) {
  encrypted_private_key = web3.eth.accounts.encrypt(pk,pw);
  console.log(encrypted_private_key);
  await localforage.setItem('keystore',encrypted_private_key);
  let keystore = await localforage.getItem('keystore');
  console.log('Local storage address keystore ');
  console.log(keystore);
  if(words){
    words_encrypted = web3.eth.accounts.encrypt(web3.utils.toHex(words),pw);
    await localforage.setItem('words_keystore',words_encrypted);
    let words_keystore = await localforage.getItem('words_keystore');
    console.log('Local storage words keystore ');
    console.log(words_keystore);
    console.log(words_keystore['address']);
    document.getElementById('words_form').style.display = 'flex';
  }


}

const doTransact = async (transaction, privatekey) => {

  console.log('About to transact');
  web3.eth.accounts.signTransaction(transaction, privatekey)
  .then(signedtx => web3.eth.sendSignedTransaction(signedtx.rawTransaction))
  .then(receipt => console.log("Transaction receipt: ", receipt))
  .then(document.getElementById('transact_to_address').value='')
  .then(document.getElementById('transact_value').value='')
  .then(document.getElementById('transact_password').value='')
  .catch(err => console.error(err));
};

function doSavedWords(){
  document.getElementById('bk_wallet').style.display = 'none';
  checkTokenKeystore();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF ENCRYPTION //////////////////////////////////////////////////////////////

async function transact(){
  let keystore = await localforage.getItem('keystore');
  //console.log('Transact pwd' + document.getElementById('transact_password').value);
  await doDecrypt(keystore,document.getElementById('transact_password').value, 'transact');
}

async function retrieveWords(){
  let words_keystore = await localforage.getItem('words_keystore');
  //console.log('Words password' + document.getElementById('words_password').value);
  let words_decrypted = await web3.eth.accounts.decrypt(words_keystore, document.getElementById('words_password').value);
  document.getElementById('words_password').value = '';
  alert(web3.utils.hexToUtf8(words_decrypted['privateKey']));
}

///////////////////////////////////////////////////////////////////////// CREATE ACCOUNT ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CREATE ACCOUNT
function doCreate(password) {
    //BIP44
    const words_seed = doGenerateSeed();
    const words = words_seed.words;
    const seed = words_seed.seed;
    const address_creation = doGenerateAddress(seed);
    const address = address_creation.address;
    const privatekey = address_creation.privatekey;
    const wallet = doManageWallet(password, privatekey);
    return {
      wallet: wallet,
      address: address,
      privatekey: privatekey,
      words: words
    };
}


function doGenerateSeed(words){
  const language = "english";
  var m = new Mnemonic(language);

  if (words == null) {
    var words = m.generate();
    console.log('Words:' + words);
  }
  console.log('Words are valid: ', m.check(words));
  var seed = m.toSeed(words);
  console.log('Seed:' + seed);

  return {
    words: words,
    seed: seed
  };
}

function doGenerateAddress(seed){

  const hdwallet = hdkey.fromMasterSeed(EthJS.Util.toBuffer('0x'+seed));
  console.log('HDWallet:' + hdwallet);

  const address = hdwallet.derivePath("m/44'/60'/0'/0/0").getWallet().getAddress().toString("hex");
  console.log('Address: 0x' + address);

  const privatekey = hdwallet.derivePath("m/44'/60'/0'/0/0").getWallet().getPrivateKey().toString("hex");
  console.log('Privatekey: 0x' + privatekey);

  const privateExtendedKey = hdwallet.privateExtendedKey();
  console.log('Private key extended: ' + privateExtendedKey);

  return {
    address: '0x' + address,
    privatekey: '0x' + privatekey,
    };
}

function doManageWallet(password,privatekey){
  var wallet = web3.eth.accounts.wallet.create();
  //Clearing might be redundant
  wallet.clear();
  wallet.add(privatekey)
  doStoreWallet(wallet,password);
  console.log('Wallet on local storage');
  console.log(localStorage.getItem('web3js_wallet'));
  return wallet;
  }

function doStoreWallet(wallet,password) {
  //wallet.encrypt(password);
  localStorage.removeItem('web3js_wallet');
  wallet.save(password);
  console.log('Storing the wallet on local storage');
}

function doExportWallet(wallet,password){
  var keystore = wallet.encrypt(password);
  return keystore;

}


function doWalletLoad(pwd,keyname){
  console.log('keyname' + keyname);
  console.log(localStorage.getItem('web3js_wallet'));
  //var wallet_keystore = web3.eth.accounts.wallet.load(pwd,keyname);
  return web3.eth.accounts.wallet.load(pwd,keyname);
}

const getAccountBalance = async (target, web3, address) => {

  const balance = await web3.eth.getBalance(address);

  document.getElementById(target).innerHTML = balance;
  return balance;
};

function getAccountWords(target, language){
  var m = new Mnemonic(language);
  var words = m.generate();
  console.log(words);
  var seed = m.toSeed(words, password);
  console.log(seed);
  if(m.check(words)){
    document.getElementById(target).innerHTML = words + "---" + seed + '---' + m.toSeed(words) + '---' + m.toSeed(words);
  }

  return words;

}
///////////////////////////////////////////////////////////////////////// KEYSTORE DOWNLOAD ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 7. BACKUP WALLET
function doBackupWalletFile() {
  downloadFile('wallet_keystore.json',localStorage.getItem('web3js_wallet'));//encrypted_private_key));
}

function doBackupWordsFile() {
  downloadFile('words_keystore.json',localStorage.getItem('words_keystore'));//encrypted_private_key));
}

// 8. DOWNLOAD FILE
function downloadFile(filename,text) {
  var element = document.createElement('a');
  element.setAttribute('href','data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download',filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
  alert('Please make sure the 12 words are noted in a secured place and are not shared for security reasons.');
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////// END OF KEYSTORE DOWNLOAD ///////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////// SIGNIN AND DECRYPT /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LOGIN
async function signin() {
  //login is done if pwd text box is not empty
  if (document.getElementById('signin_password').value != '') {
    console.log('Sign with password');
    //if importedFile exists, it is sent to do the decrypt
    var wallet;
    var address;
    var privatekey;
    if (importedFile !=''){
      console.log('Signing in with wallet keystore file: ');
      remove_words();
      wallet = web3.eth.accounts.wallet.decrypt(JSON.parse(importedFile),document.getElementById('signin_password').value);
      //let keystore = doWalletLoad(document.getElementById('signin_password').value,'web3js_wallet');//await localforage.getItem('keystore');
    }else{
      console.log('Signing in with local storage wallet keystore: ');
      wallet = doWalletLoad(document.getElementById('signin_password').value,'web3js_wallet');
      //await localforage.getItem('keystore');
    }
    console.log('The wallet ');
    console.log(wallet);

    address = wallet['0']['address'];
    console.log('Address: ' + address);

    privatekey = wallet['0']['privateKey'];
    console.log('Privatekey: ' + privatekey);
    //12 words are procided
  }  else if (document.getElementById('signin_words').value != ''){

      console.log('Signing in with 12 words:');
      const words_seed = await doGenerateSeed(document.getElementById('signin_words').value);
      const seed = words_seed.seed;
      const address_creation = await doGenerateAddress(seed);
      address = address_creation.address;
      privatekey = address_creation.privatekey;
    }

    //API login using fetch
    var serialized = create_sig(sign_string,privatekey);
    const url = script_root + '/login';
    console.log('Address: ' + address)
    console.log('Signature: ' + serialized)
    const data = {
      username : address,
      sig : serialized
    };
    const param = {
      headers : {"content-type" : "application/json;charset=UTF-8"},
      body:JSON.stringify(data),
      method: "POST",
      json:true
    };
    fetch(url,param)
    .then(res => res.json())
    .then(function(data){
      if(data['access_token'] != 'NA') {
        save_to_local_forage(data['access_token'],'token');
        if (importedFile != ''){
          console.log('Signing in with uploaded file');
          doEncrypt(privatekey,document.getElementById('signin_password').value);
          doManageWallet(document.getElementById('signin_words_password').value, privatekey);
          document.getElementById('signin_password').value = '';
          document.getElementById('file-input').value = '';
          importedFile = '';
        //Signing with words
        }else if (document.getElementById('signin_words').value != ''){
          console.log('Signing in with words');
          //Setting address keystore storage
          //let words_ks = web3.eth.accounts.encrypt(web3.utils.toHex(document.getElementById('signin_words').value),document.getElementById('signin_words_password').value);
          //console.log('===Words Encrypted==');
          //console.log(web3.utils.toHex(document.getElementById('signin_words').value));
          //console.log('====');
          //console.log(words_ks);
          //let words_dec = web3.eth.accounts.decrypt(words_ks, document.getElementById('signin_words_password').value);
          //console.log('===Words Decrypted===');
          //console.log(words_dec);
          //console.log(web3.utils.hexToUtf8(words_dec['privateKey']));
          //Encrypting the address and the words
          doEncrypt(privatekey,document.getElementById('signin_words_password').value,document.getElementById('signin_words').value);
          doManageWallet(document.getElementById('signin_words_password').value, privatekey);
          document.getElementById('signin_words_password').value = '';
          document.getElementById('signin_words').value = '';
        //Sign in with wallet local storage
        } else{
          console.log('Signing in with local storage');
          //Just in case no address keystore is stored
          doEncrypt(privatekey,document.getElementById('signin_password').value);
          document.getElementById('signin_password').value = '';
        }
        //registration div is hidden
        document.getElementById('signin').style.display = 'none';
        //download file div is displayed
        document.getElementById('dentro').style.display = 'block';
        document.getElementById('user').innerHTML = data['username'];
        document.getElementById('token').innerHTML = data['access_token'];
        getAccountBalance("balance", web3, address);
        //getAccountWords("words", "english");
      }
      else {
        alert(data['message']);
      }
    })
    .catch(error => console.log('Error!! '+error))
  }

//FUNCTION THAT CREATES A SIGNATURE ON A TEXT
function create_sig(sign_string,pk) {
  var pk_buffer = EthJS.Util.toBuffer(pk);
  var message = EthJS.Util.toBuffer(sign_string);
  var msgHash = EthJS.Util.hashPersonalMessage(message);
  console.log('Message hash: ' + msgHash);
  var sig = EthJS.Util.ecsign(msgHash,pk_buffer);
  console.log('Signature: ' + sig);
  var serialized = EthJS.Util.bufferToHex(EthSig.Util.concatSig(sig.v,sig.r,sig.s));
  console.log('Signature hex: ' + serialized);
  return serialized;

}

//DECRYPT PRIVATE KEY
async function doDecrypt(keystore,p,action) {
  console.log('Decrypting');
  decrypted_private_key = web3.eth.accounts.decrypt(keystore, p);

  if(action=='transact'){
    console.log('Triggering transaction');
    var transaction = {
      to: document.getElementById('transact_to_address').value,
      value: web3.utils.toHex(web3.utils.toWei(document.getElementById('transact_value').value, 'ether')),
      gas: 21000,
      gasLimit: 21000,
      gasPrice: web3.utils.toHex(web3.utils.toWei('0.000000001', 'ether'))
    };
    console.log('The transaction:');
    console.log(transaction);
    doTransact(transaction, decrypted_private_key['privateKey']);
  }else{
    //API login using fetch
    var serialized = create_sig(sign_string,decrypted_private_key['privateKey']);
    const url = script_root + '/login';
    console.log('Decrypted account: ' + decrypted_private_key['address'])
    console.log('Signature: ' + serialized)
    const data = {
      username : decrypted_private_key['address'],
      sig : serialized
    };
    console.log('data:' + data);
    const param = {
      headers : {"content-type" : "application/json;charset=UTF-8"},
      body:JSON.stringify(data),
      method: "POST",
      json:true
    };
    fetch(url,param)
    .then(res => res.json())
    .then(function(data){
      if(data['access_token'] != 'NA') {
        save_to_local_forage(data['access_token'],'token');
        if (importedFile != ''){
          doEncrypt(decrypted_private_key['privateKey'],p);
        }
        //registration div is hidden
        document.getElementById('signin').style.display = 'none';
        //download file div is displayed
        document.getElementById('dentro').style.display = 'block';
        document.getElementById('user').innerHTML = data['username'];
        document.getElementById('token').innerHTML = data['access_token'];
        getAccountBalance("balance", web3, username);
        //getAccountWords("words", "english");
      }
      else {
        alert(data['message']);
      }
    })
    .catch(error => console.log('Error: '+error))
  }


}



// 4. IMPORT THE KEYSTORE
//if the element file-input changes, that means that the import keystore button has been clicked and that triggers readSingleFile
document.getElementById('file-input').addEventListener('change', readSingleFile, false);
function readSingleFile(evt) {
  // importedFile is emptied
  importedFile = '';
  // Check for the various File API support.
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0];
    if (f) {
      var r = new FileReader();
      r.onload = function(e) {
	      importedFile = e.target.result;
      }
      r.readAsText(f);
    } else {
      alert("Failed to load file");
    }
  }
  else {
    alert('The File APIs are not fully supported by your browser.');
  }
}


function save_to_local_forage(d,n) {
  if(n == 'token' & d != 'NA') {
    localforage.setItem(n,d,function(err,result){
      if (err) {
        console.error('Error writting to localforage');
      }
      else {
        console.log('Token: ' + JSON.stringify(result) + ' written to localforage');
        console.log('=================================================');
      }
    });
  }
  else{
    console.log('No token available');
  }
}


//Checks if the words keystore is for the given address
async function checkWordsAddress(address){

  console.log('Checking if the words keystore match the wallet/address keystore');
  console.log(address);
  let words_keystore = await localforage.getItem('words_keystore');
  console.log(words_keystore);
  if (words_keystore){
    if (address != words_keystore['address']){
      remove_words();
    } else{
      console.log('The words keystore matches the address as they refer to a different address');
    }
  }


}






//UNLOCKACCOUNT --> NOT BEING USED
function    doUnlockAccount()  {
  web3.personal.unlockAccount(account, pwd,function(error, result)  {
        // console.log(error,result)
        if(error){
            alert('Unlock error ' + error);
            //setData('lock_unlock_result',error,true);
        } else {
            // Result = True if unlocked, else false
            var str = account.substring(0,20)+'...Unlocked';
            if(result){
                console.log('Unlock: '+ str);
                //setData('lock_unlock_result',str,false);
            } else {
                // This does not get called - since and error is returned for incorrect password :-)
                str = 'Incorrect Password???';
                console.log('Unlock: '+ str);
                //setData('lock_unlock_result',str,true);
            }
        }
    });
}
