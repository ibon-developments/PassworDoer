# PassworDoer

Create/backup/restore an ETH address based on BIP44. Passwordless session login and token handling using JWT

There are two flavours:
* ETH address generation/retrieval (12 words). Save mnemonics, address and wallet keystore on local storage. Message signature encryption (passwordless authentication) sent to the backend [passwordoer_sig.py]
* (Deprecated) ETH address and password (regular authentication) sent to the backend [passwordoer.py]

## Architecture
* Frontend has the functions to manage the ETH accounts, store the address and wallet keystores and the account session token
* Backend has a SQLAlchemy to validate ETH accounts and its signatures and handle the sessions

## Main functionalities
* Generate an ETH address based on 12 words mnemonic (BIP44):  generate the address from a randomly generated mnemonic words. Based on the provided password, generate wallet, address and mnemonic keystores and store them on local storage the keystore. Provide the wallet and words keystore for user backup download. Fetch the session details from the backend whether crafted randomly or user provided
* Restore a wallet stored on local storage: decrypt the wallet keystore available on local storage with the provided password. Fetch the session details from the backend.
* Backup the ETH address wallet keystore: save the keystore on a target destination
* Restore the ETH address wallet keystore from external file: load the file and decrypt along the provided password. Store on the local storage the keystore of the wallet and generated address and fetch the session details from the backend
* Restore the ETH address from mnemonic: retrieve the address from the provided words. Based on the provided password, generate wallet, address and mnemonic keystores and store them on local storage the keystore and fetch the session details from the backend
* Show the mnemonics (12 words) if available on the local storage
* Fetch the session details from the backend based on the ETH account details
* Session login: read the token keystore on the local storage and request the password to decrypt with expiration time
* Delete the wallet keystore from local storage: delete the wallet keystore on the local storage
* Delete the address keystore from local storage: delete the address keystore on the local storage (localForage)
* Destroy the session token: flag the session value on the backend as unusable
* Sign and send a transaction with the ETH address in use providing destination address, value and the account keystore decryption password.


## Requirements

* Python 3.6
* [bignumber](https://github.com/MikeMcl/bignumber.js/)
* [crypto-js](https://github.com/brix/crypto-js)
* [eth-sig-util](https://github.com/MetaMask/eth-sig-util)
* [ethereumjs-util](https://github.com/ethereumjs/ethereumjs-util)
* [hdkey](https://github.com/ethereumjs/ethereumjs-wallet#hd-wallet-api)
* [jsbip39](https://github.com/iancoleman/jsbip39)
* [localForage](https://github.com/localForage/localForage)
* [web3js](https://github.com/ethereum/web3.js/)

## Steps

* $ git clone https://gitlab.com/ibon-developments/PassworDoer.git
* $ virtualenv -p python3 PassworDoer/
* $ source bin/activate
* (PassworDoer)$ pip install -r requirements.txt
* For ETH account and password credentials sent to the backend
  (PassworDoer)$ python passwordoer.py
* For ETH account and ETH signature sent to the backend
  (PassworDoer)$ python passwordoer_sig.py

Server should appear on https://127.0.0.1:5000


## Application flow
# Sign up
1. User provides a password to be used for encryption
2. 12 mnemonic words are randomly generated
3. Based on the mnemonic, an ETH address details are retrieved
4. A wallet instance is wiped to store only the current account
5. The wallet is encrypted and stored on the local storage to be used for session handling
6. The address is encrypted and stored on the local storage (localForage) for transaction management
7. (If available) The mnemonics are encrypted and stored on the local storage (localForage) for future retrieval
8. The address details are sent to the backend for JWT session handling
9. The user is prompted to download the encrypted keystore of the wallet as backup
10. The user is invited to write down the mnemonic words for account restore
11. The address, balance, session token, privatekey, mnemonic words are shown. A transaction can be performed providing destination address, value and the address keystore decryption password.

# Sign in
1. If there is a wallet in the local storage, the user is prompted to provide the decryption password
1.1. Upon decryption success, follows from step [Sign up 8]
2. If no wallet is on the local storage, the user can either
2.1. upload a keystore file along the decryption password
2.1.1. Remove the words keystore on local storage
2.1.2. Upon successful decryption, follows from step [Sign up 4]
2.2. type the 12 mnemonic words in order to restore the account along a decryption password
2.2.1. Upon form is sent, follows from step [Sign up 3]
