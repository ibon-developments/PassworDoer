from __future__ import print_function
from flask import Flask, request
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, reqparse
from flask_jwt_extended import JWTManager
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from flask_cors import CORS, cross_origin
from passlib.hash import pbkdf2_sha256 as sha256
from web3.auto import w3
from eth_account.messages import defunct_hash_message

sign_message = 'Hello World'

import sys

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'some-secret-string'
db = SQLAlchemy(app)



app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
jwt = JWTManager(app)

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']



##################################################################### MODELS ###############################################################################
class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(120), unique = True, nullable = False)
    password = db.Column(db.String(120), nullable = False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls,username):
        return cls.query.filter_by(username = username.upper()).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username,
                'password': x.password
                }
        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key = True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)

##############################################################################################################################
##############################################################################################################################




#allow CORS
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin','*')
    response.headers.add('Access-Control-Allow-Headers','Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods','GET,PUT,POST,DELETE')
    return response

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)

@app.route('/')
def home():
    return render_template('home_sig.html')

@app.route('/registration',methods=['POST'])
def registration():
    data = request.data
    dataDict = json.loads(data.decode('utf-8'))
    #return json.dumps(dataDict, ensure_ascii=False)

    #username = dataDict['username']
    #data = json.loads(data)


    if UserModel.find_by_username(dataDict['username']):
        return json.dumps({'message': 'User {} already exists'.format(dataDict['username']),'access_token':'NA'},ensure_ascii=False)

    new_user = UserModel(
        username = dataDict['username'].upper(),
        password = UserModel.generate_hash('borrar_campo_bs')
    )
    print('usuario: '+ dataDict['username'],file=sys.stdout)
    #cambio print('pwd: ' + dataDict['password'],file=sys.stdout)

    try:
        new_user.save_to_db()
        access_token = create_access_token(identity = dataDict['username'])
        refresh_token = create_refresh_token(identity = dataDict['username'])
        return json.dumps({
            'message': 'User {} was created'.format(dataDict['username']),
            'access_token': access_token,
            'refresh_token': refresh_token
            },ensure_ascii=False)
    except:
        return json.dumps({'message': 'Something went wrong','access_token':'NA'},ensure_ascii=False), 500

@app.route('/login', methods = ['POST'])
def post():
    data = request.data
    dataDict = json.loads(data.decode('utf-8'))

    current_user = UserModel.find_by_username(dataDict['username'].upper())

    if not current_user:
        return json.dumps({'message': 'User {} doesn\'t exist'.format(dataDict['username']),'access_token':'NA','refresh_token':'NA'},ensure_ascii=False)


    signer = w3.eth.account.recoverHash(defunct_hash_message(text=sign_message),signature=dataDict['sig'])
    #signer = w3.eth.account.recoverHash(defunct_hash_message(text='Hello World'),signature='0x392a8d35bd87650828edf327b8f6bbd9e8c4e65b5062e7abc730af0cd175a1ad7607b54916580a48be0e64982c331cbd328a113d0c3abc2c3ead069175884f4a1b')
    print('User: '+ dataDict['username'],file=sys.stdout)
    print('Signer: '+ signer,file=sys.stdout)
    if (signer.upper() == dataDict['username'].upper()):#esto esssss muy raro..... de pronto el siiigner cambia ellllllll case del account
        print('Creating and access token')
        access_token = create_access_token(identity = dataDict['username'])
        print('Access token: ' + access_token)
        refresh_token = create_refresh_token(identity = dataDict['username'])
        return json.dumps({
            'message': 'Logged in as {}'.format(current_user.username),
            'username': current_user.username,
            'access_token': access_token,
            'refresh_token': refresh_token
            },ensure_ascii=False)
    else:
        return json.dumps({'message': 'Wrong credentials','access_token':'NA','refresh_token':'NA'},ensure_ascii=False)

@app.route('/gettoken', methods=['GET'])
@jwt_required
def get():
    if request.method == 'GET':
        print('hemos entrado',file=sys.stdout)
    return json.dumps({
        'answer': 42,
        'username': get_jwt_identity()
        }, ensure_ascii=False)

@app.route('/logout', methods=['POST'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    try:
        revoked_token = RevokedTokenModel(jti = jti)
        revoked_token.add()
        return json.dumps({'message': 'Access token has been revoked'}, ensure_ascii=False)
    except:
        return json.dumps({'message': 'Something went wrong'}, ensure_ascii=False), 500


if __name__ == '__main__':
    app.run(port=5005,debug=True)
